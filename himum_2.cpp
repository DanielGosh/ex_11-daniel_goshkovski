#include <iostream>
#include <thread>
#include <vector>
#include <string>
#include <fstream>
#include <chrono>

void printVector(std::vector<int> primes);
void getPrimes(int begin, int end, std::vector<int>& primes);
std::vector<int> callGetPrimes(int begin, int end);

int main()
{
	int begin = 0, end = 0;
	std::cout << "enter start of series: ";
	std::cin >> begin;
	std::cout << "enter end of series: ";
	std::cin >> end;
	std::vector<int> p = callGetPrimes(begin, end);
	printVector(p);
	return 0;
}
/*
function that prints the contents of the vector by iterating over
it's indexes
parameter: std::vector<int> primes
return value: none
*/
void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		std::cout << "value at [" << i << "] : " << primes[i] << ",\n";
	}
}
/*
function that searches for prime numbers in a set series of numbers.
parameters: int begin(first value of the series), int end(last valaue of the series), std::vector<int>& primes(vector to put the prime numbers
into)
return value: none
*/

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int beginL = begin;
	int endL = end;
	int i = 0;
	bool primeBool = true;
	while (beginL < endL) {
		primeBool = true;
		if (beginL == 0 || beginL == 1) //incase there isnt a series(at least not a viable one if you count it as a series)
		{
			primeBool = false;
		}
		else {
			for (i = 2; i <= beginL / 2; ++i) {
				if (beginL % i == 0) {
					primeBool = false;
					break;
				}
			}
		}
		if (primeBool) {
			primes.push_back(beginL);
		}
		++beginL;
	}
}
/*
function that calls for the prime number search, calculates the time and prints it(while creating a thread of course)
parameters: int begin, int end
return value: the vector of the prime numbers
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now(); // starting the time counting
	std::vector<int> pr;
	std::thread a(getPrimes, begin, end, std::ref(pr));
	a.join();
	std::chrono::steady_clock::time_point finish = std::chrono::steady_clock::now(); // end of time counting
	std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count() << "[micro]" << std::endl;
	return pr;
}