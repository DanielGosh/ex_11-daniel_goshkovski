#include <iostream>
#include <thread>
#include <vector>
#include <string>
#include <fstream>
#include <chrono>

void writePrimesToFile(int begin, int end, std::ofstream& f);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

int main()
{
	int begin = 0, end = 0, split = 0;
	std::cout << "enter start of series: ";
	std::cin >> begin;
	std::cout << "enter end of series: ";
	std::cin >> end;
	std::cout << "enter amount of split serieses to make: ";
	std::cin >> split;
	callWritePrimesMultipleThreads(begin, end, "C:\\messimot\\bdihamin.txt", split);
	return 0;
}
/*
function that searches for prime numbers in a set series of numbers and writes it into a file.
parameters: int begin(first value of the series), int end(last valaue of the series), std::ofstream& f(the file we write it into)
return value: none
*/
void writePrimesToFile(int begin, int end, std::ofstream& f)
{
	int beginL = begin;
	int endL = end;
	int i = 0;
	bool primeBool = true;
	while (beginL < endL) {
		primeBool = true;
		if (beginL == 0 || beginL == 1) {
			primeBool = false;
		}
		else {
			for (i = 2; i <= beginL / 2; ++i) {
				if (beginL % i == 0) {
					primeBool = false;
					break;
				}
			}
		}
		if (primeBool) {
			f << beginL;
		}
		++beginL;
	}
}

/*
function that calls for the prime number search, calculates the time and writes it into a file(while creating seperate threads for it)
parameters: int begin, int end, std::string filePath(path of the file to write into), int N(split the series into N pieces)
return value: none
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream f(filePath, std::ofstream::out);
	int each = end / N;
	int beginL = begin;
	int endL = beginL + each - 1;
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now(); // start time counting
	for (int i = 0; i < N; i++)
	{
		std::thread a(writePrimesToFile, beginL, endL, std::ref(f));
		a.join();
		beginL += each;
		endL += each;
	}
	std::chrono::steady_clock::time_point finish = std::chrono::steady_clock::now(); // end of time counting
	std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>((finish - start)).count() << "[micro]" << std::endl;
}